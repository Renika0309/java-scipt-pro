
//Створюю клас Driver. Клас Driver містить поля - ПІБ, стаж водіння.

 class Driver   {
    constructor(fullName, experience){
       this.fullName = fullName;
       this.experience = experience;
    }
 }
//  Створюю клас Engine. Клас Engine містить поля – потужність, виробник.
 
 class Engine extends Driver {
    constructor(power,company,fullName,experience){
        super(fullName,experience);
        this.power = power;
        this.company = company;
    }}

   /*  Створюю клас Car. Клас Car містить поля – марка автомобіля, клас автомобіля, вага,
 водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(),
  які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або 
  "Поворот ліворуч". А також метод toString(), який виводить повну інформацію
   про автомобіль, її водія і двигуна.  */
 class Car extends Engine{
   constructor(power,company,fullName,experience, marka, carClass,weight,img){
    super(power,company,fullName,experience);
    this.marka = marka;
    this.carClass = carClass;
    this.weight = weight;
    this.img = img;
   }
   start(){
    document.write(`<input type = "button" value = "Поїхали">`)
   }
   stop(){
    document.write(`<input type = "button" value ="Зупиняємося" >`)
   }
   turnRight(){
    document.write(`<input type = "button" value ="Поворот праворуч" >`)
   }
   turnLeft(){
    document.write(`<input type = "button" value = "Поворот ліворуч">`)
   }
   toString(){
    for(const key in this){
        document.write(`<ul> <li>${key} : ${this[key]}</li></ul> `)
    }
   }
 };
 //Створюю похідний від Car клас - Lorry (вантажівка), що характеризується 
//також вантажопідйомністю кузова.
 class Lorry extends Car{
    constructor(power,company,fullName,experience, marka, carClass,weight,img,carrying){
        super(power,company,fullName,experience, marka, carClass,weight,img);
        this.carrying = carrying;
    }
 }

 //Створюю похідний від Car клас - SportCar, який також характеризується
 //граничною швидкістю

 class SportCar extends Car{
    constructor(power,company,fullName,experience, marka, carClass,weight,img,speed){
        super(power,company,fullName,experience, marka, carClass,weight,img);
        this.speed = speed;
    }
 }
 
 
const driver = new Driver ("Slavik Renata Stanislavivna", "3 year");
console.log(driver);

const engine = new Engine( "165 кВт", "шведской компанией Volvo Cars");
console.log(engine);

const volvo = new Car( "165 кВт",
 "шведской компанией Volvo Cars",
 "Slavik Renata Stanislavivna",
 "3 year",
 "Volvo XC90", 
 "SUV /кроссовер" ,
 "від 1936 до 2415 кг",
 `<img src = "http://ipravda.sk/res/2019/03/05/thumbs/volvo-xc90-2019_14-clanokW.jpg"`);

volvo.toString();
volvo.start();
volvo.stop();
volvo.turnLeft();
volvo.turnRight();


const mercedes = new Lorry("326л/с , Euro 5",
 "Mercedes-Benz",
 "Shtymak Ivan Ivanovyc", 
 "7 year",
"Merceds-Benz Zetros 2733 6×6",
 "Грузовик" ,
 "від 25 до 27 тон",
 `<img src = "https://o.aolcdn.com/images/dims3/GLOB/legacy_thumbnail/800x450/format/jpg/quality/85/http://www.blogcdn.com/www.autoblog.com/media/2011/03/web630-82510515227964961330711c268009.jpg">`,
"16.5 тон");

mercedes.toString();
mercedes.start();
mercedes.stop();
mercedes.turnLeft();
mercedes.turnRight();


const ferrari = new SportCar(" 670 л.с",
 "Ferrari",
 "Shtymak Branislav Volodymorovyc", 
 "5 year",
"Ferrari 488",
 "Спортивні машини" ,
 "1475 кг",
 `<img src = "https://autoline.sk/img/s/automobil-kupe-FERRARI-488-GTB---1650397543929998089_big--22041922415493550000.jpg"`,
 "330 км/ч");
 
ferrari.toString();
ferrari.start();
ferrari.stop();
ferrari.turnLeft();
ferrari.turnRight();
